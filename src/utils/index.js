export function createElement(tagName, parent, attributes = {}, content = '') {
    try {
        const element = document.createElement(tagName);
        Object.keys(attributes).forEach(key => {
            element.setAttribute(key, attributes[key]);
        });

        element.textContent = content;

        parent.appendChild(element);
        return element;
    } catch(error) {
        console.error(`Error creating element with tag ${tagName} with attributes  ${JSON.stringify(attributes)}. Error: ${error}`);
    }
}