export const mockdata = {
    cart: [{
        title: 'Test',
        price: '749'
    }],
    items: [
        {
            title: 'NIKE CRYPTOKICK',
            price: 333,
            rating: 4,
            maxRating: 5,
            src: './assets/items/nike-cryptokick-1.svg',
            titleColor: 'black',
            titleTextStrokeWidth: '4px',
            description: 'A Woocommerce product gallery Slider for Slider\nRevolution with mind-blowing visuals.',
            width: '700px',
            height: '360px',
        },
        {
            title: 'NIKE CRYPTOKICK',
            price: 5000,
            rating: 4,
            maxRating: 5,
            src: './assets/items/nike-cryptokick-2.svg',
            titleTextStrokeWidth: '4px',
            titleColor: 'black',
            description: 'A Woocommerce product gallery Slider for Slider\nRevolution with mind-blowing visuals.',
            width: '670px',
            height: '400px'
        },
        {
            title: 'NIKE AIR JORDAN',
            price: 228,
            rating: 4,
            maxRating: 5,
            src: './assets/items/nike-air-jordan.svg',
            titleTextStrokeWidth: '2px',
            titleColor: 'white',
            description: 'A Woocommerce product gallery Slider for Slider\nRevolution with mind-blowing visuals.',
            width: '640px',
            height: '420px'
        },
        {
            title: 'NIKE MAG BACK TO THE FUTURE',
            price: 999,
            rating: 4,
            maxRating: 5,
            src: './assets/items/nike-mag.svg',
            titleTextStrokeWidth: '2px',
            titleColor: 'white',
            description: 'A Woocommerce product gallery Slider for Slider\nRevolution with mind-blowing visuals.',
            width: '600px',
            height: '500px'
        }
    ]
};
