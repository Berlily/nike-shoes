export const images = {
    socialMediaNavigation: [
        {alt: 'twitter', src:'./assets/social-media/twitter.svg'},
        {alt: 'instagram', src: './assets/social-media/instagram.svg'},
        {alt: 'facebook', src: './assets/social-media/facebook.svg'},
    ],
    sidebar: {
        leftArrow: './assets/left-arrow.svg',
        rightArrow: './assets/right-arrow.svg',
    },
    item: {
        filledStar: './assets/filled-star.svg',
        notFilledStar: './assets/not-filled-star.svg',
    }
};
