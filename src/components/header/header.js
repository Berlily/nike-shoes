import { createElement } from '../../utils';
import { images } from '../../api/images';
import { content } from '../../api/content';
import { mockdata } from '../../api/mockdata';

import './header.css'

export function setupHeader(parent) {
    const header = createElement('header', parent, {
        class: 'header',
    });

    createElement('img', header, {
        class: 'header__logo',
        src: './assets/logo.svg'
    });

    const navbar = createElement('div', header, {
        class: 'nav'
    });

    const navbarItems = content.navbarItems;

    const navbarList = createElement('ul', navbar, {
        class: 'nav__list',
    });

    navbarItems.forEach(itemText => createElement('li', navbarList, {
        class: 'nav__item',
    }, itemText));

    const socialMediaNav = createElement('div', header, {
        class: 'media-nav'
    });

    const socialMediaNavItems = Object.values(images.socialMediaNavigation);

    const socialMediaNavList = createElement('ul', socialMediaNav, {
        class: 'media-nav__list',
    });

    socialMediaNavItems.forEach(({src, alt}) => {
        const element = createElement('li', socialMediaNavList, {
            class: 'nav__item',
        });
        createElement('img', element, {
            src,
            alt
        });
    });

    createElement('span', header, {
        class: 'header__cart-info',
    }, `CART (${mockdata.cart.length})`);

    return header;
}
