import { createElement } from '../../../utils';
import { images } from '../../../api/images';
import { switchItem } from '../../../utils/item-switcher';
import './sidebar.css';

export function setupSidebar(parent) {
    const sidebar = createElement('div', parent, {
        class: 'sidebar',
    });

    const leftSidebar = createElement('div', sidebar, {class: 'sidebar__circle'});
    const rightSidebar = createElement('div', sidebar, {class: 'sidebar__circle'});

    leftSidebar.addEventListener('click', () => {
        switchItem(parent, 'left');
    });

    rightSidebar.addEventListener('click', () => {
        switchItem(parent, 'right');
    });

    createElement('img', leftSidebar, {class: 'sidebar__arrow', src: images.sidebar.leftArrow});
    createElement('img', rightSidebar, {class: 'sidebar__arrow', src: images.sidebar.rightArrow});

    return sidebar;
}
