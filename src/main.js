import { setupHeader } from './components/header/header';
import { setupMain } from './components/main/main';
import { setupSidebar } from './components/main/sidebar/sidebar';
import { switchItem } from './utils/item-switcher';

import './style.css';

const app = document.querySelector('#app');
setupHeader(app);
const main = setupMain(app);
setupSidebar(main);

// Setup one of the first items from server
switchItem(main, 'right');

